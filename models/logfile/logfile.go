package logfile

type LogFile struct {
	FileName   string
	Lines      []LogLine
	LineNumber int
}
type LogLine struct {
	Timestamp    string
	IP           string
	Username     string
	Database     string
	ProcessID    string
	LogType      string
	PreStatement string
	Statement    string
}
