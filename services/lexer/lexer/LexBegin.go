package lexer

import (
	"github.com/securityscorecard/rds-pgreplay/services/errors"
	"github.com/securityscorecard/rds-pgreplay/services/lexer/lexertoken"
	"log"
	"regexp"
	"strings"
	"time"
)

const POSTGRESQL_DATETIME = "2006-01-02 15:04:05 UTC"

// Begins and sets it to Date
func LexBegin(lexer *Lexer) LexFn {
	lexer.SkipWhitespace()
	for {
		if lexer.IsEOF() {
			return lexer.Errorf(errors.LEXER_ERROR_MISSING_COLON_ONE)
		}
		if strings.HasPrefix(lexer.InputToEnd(), lexertoken.COLON) {
			// Check condition of COLON
			if _, err := time.Parse(POSTGRESQL_DATETIME, strings.TrimSpace(lexer.Input[lexer.Start:lexer.Pos])); err == nil {
				lexer.Emit(lexertoken.TOKEN_TIMESTAMP)
				return LexColonOne
			}
		}
		lexer.Inc()

	}
}
func LexColonOne(lexer *Lexer) LexFn {
	lexer.Pos += len(lexertoken.COLON)
	lexer.Emit(lexertoken.TOKEN_COLON)
	return LexIP
}

func LexIP(lexer *Lexer) LexFn {
	for {
		// Check if colon. If so, no ip
		if strings.HasPrefix(lexer.InputToEnd(), lexertoken.COLON) {
			lexer.Emit(lexertoken.TOKEN_REMOTE_HOST)
			return LexColonTwo
		}
		if lexer.Input[lexer.Pos:lexer.Pos+1] == lexertoken.COLON {
			return LexColonTwo
		}
		lexer.Inc()

		if lexer.IsEOF() {
			return lexer.Errorf(errors.LEXER_ERROR_MISSING_COLON_TWO)
		}
	}
}

func LexColonTwo(lexer *Lexer) LexFn {
	lexer.Pos += len(lexertoken.COLON)
	lexer.Emit(lexertoken.TOKEN_COLON)
	return LexUsername
}
func LexUsername(lexer *Lexer) LexFn {
	for {
		if strings.HasPrefix(lexer.InputToEnd(), lexertoken.AT) {
			lexer.Emit(lexertoken.TOKEN_USERNAME)
			return LexAtSign
		}
		if lexer.Input[lexer.Pos:lexer.Pos+1] == lexertoken.AT {
			return LexAtSign
		}
		lexer.Inc()
		if lexer.IsEOF() {
			return lexer.Errorf(errors.LEXER_ERROR_MISSING_AT_SIGN)
		}
	}
}
func LexAtSign(lexer *Lexer) LexFn {
	lexer.Pos += len(lexertoken.AT)
	lexer.Emit(lexertoken.TOKEN_AT_SIGN)
	return LexDatabaseName
}
func LexDatabaseName(lexer *Lexer) LexFn {
	for {
		if strings.HasPrefix(lexer.InputToEnd(), lexertoken.COLON) {
			lexer.Emit(lexertoken.TOKEN_DATABASE)
			return LexColonThree
		}
		if lexer.Input[lexer.Pos:lexer.Pos+1] == lexertoken.COLON {
			return LexColonThree
		}
		lexer.Inc()
		if lexer.IsEOF() {
			return lexer.Errorf(errors.LEXER_ERROR_MISSING_COLON_THREE)
		}
	}
}
func LexColonThree(lexer *Lexer) LexFn {
	lexer.Pos += len(lexertoken.COLON)
	lexer.Emit(lexertoken.TOKEN_COLON)
	return LexAfterColonThree
}
func LexAfterColonThree(lexer *Lexer) LexFn {
	if strings.HasPrefix(lexer.InputToEnd(), lexertoken.LEFT_BRACKET) {
		return LexLeftBracket
	} else {
		return lexer.Errorf(errors.LEXER_ERROR_MISSING_LEFT_BRACKET)
	}
}
func LexLeftBracket(lexer *Lexer) LexFn {
	lexer.Pos += len(lexertoken.LEFT_BRACKET)
	lexer.Emit(lexertoken.TOKEN_LEFT_BRACKET)
	return LexProcessID
}
func LexProcessID(lexer *Lexer) LexFn {
	for {
		if lexer.IsEOF() {
			return lexer.Errorf(errors.LEXER_ERROR_MISSING_RIGHT_BRACKET)
		}
		if strings.HasPrefix(lexer.InputToEnd(), lexertoken.RIGHT_BRACKET) {
			lexer.Emit(lexertoken.TOKEN_PROCESS_ID)
			return LexRightBracket
		}
		lexer.Inc()
	}
}
func LexRightBracket(lexer *Lexer) LexFn {
	lexer.Pos += len(lexertoken.RIGHT_BRACKET)
	lexer.Emit(lexertoken.TOKEN_RIGHT_BRACKET)
	return LexAfterRightBracket
}
func LexAfterRightBracket(lexer *Lexer) LexFn {
	if strings.HasPrefix(lexer.InputToEnd(), lexertoken.COLON) {
		return LexColonFour
	} else {
		return lexer.Errorf(errors.LEXER_ERROR_MISSING_COLON_FOUR)
	}
}
func LexColonFour(lexer *Lexer) LexFn {
	lexer.Pos += len(lexertoken.COLON)
	lexer.Emit(lexertoken.TOKEN_COLON)
	return LexLogType
}
func LexLogType(lexer *Lexer) LexFn {
	for {
		if lexer.IsEOF() {
			return lexer.Errorf(errors.LEXER_ERROR_MISSING_COLON_FIVE)
		}
		if strings.HasPrefix(lexer.InputToEnd(), lexertoken.COLON) {
			lexer.Emit(lexertoken.TOKEN_LOG_TYPE)
			return LexColonFive
		}
		lexer.Inc()
	}
}
func LexColonFive(lexer *Lexer) LexFn {
	lexer.Pos += len(lexertoken.COLON)
	lexer.Emit(lexertoken.TOKEN_COLON)
	return LexPreStatement
}
func LexPreStatement(lexer *Lexer) LexFn {
	for {
		if lexer.IsEOF() {
			return lexer.Errorf(errors.LEXER_ERROR_MISSING_COLON_SIX)
		}
		if lexer.Input[lexer.Pos:lexer.Pos+1] == "\n" {
			lexer.Emit(lexertoken.TOKEN_SQL_STATEMENT)
			return LexBegin
		}
		if strings.HasPrefix(lexer.InputToEnd(), lexertoken.COLON) {
			lexer.Emit(lexertoken.TOKEN_PRE_STATEMENT)
			return LexColonSix
		}
		lexer.Inc()
	}
}
func LexColonSix(lexer *Lexer) LexFn {
	lexer.Pos += len(lexertoken.COLON)
	lexer.Emit(lexertoken.TOKEN_COLON)
	return LexSQLStatement
}
func LexSQLStatement(lexer *Lexer) LexFn {
	for {

		//if lexer.Input[lexer.Pos:lexer.Pos+3] == "\n" && lexer.Pos+1 is last
		// This means end of file without semi colon
		if lexer.Input[lexer.Pos:lexer.Pos+1] == "\n" && len(lexer.Input) == lexer.Pos+1 {
			lexer.Emit(lexertoken.TOKEN_SQL_STATEMENT)
			return LexRareEOF
		}

		// This only works if inbounds
		if lexer.Pos+3 <= len(lexer.Input) {
			// Test if Starts with 20XX- (marking year). Very unlikely to match this.
			// Would be tab, space, etc.
			// Needs to match
			if lexer.Input[lexer.Pos:lexer.Pos+3] == "\n20" {
				// Stricter match
				matched, err := regexp.MatchString("^\n20\\d\\d-$", lexer.Input[lexer.Pos:lexer.Pos+6])
				if err != nil {
					log.Println(err)
					return lexer.Errorf(err.Error())
				}
				if matched {
					lexer.Emit(lexertoken.TOKEN_SQL_STATEMENT)
					return LexBegin
				}
			}
		}

		if strings.HasPrefix(lexer.InputToEnd(), lexertoken.SEMI_COLON) {
			//log.Println(lexer.Input[lexer.Start:lexer.Pos])
			lexer.Emit(lexertoken.TOKEN_SQL_STATEMENT)
			return LexSemiColon
		}
		lexer.Inc()

		if lexer.IsEOF() {
			return lexer.Errorf(errors.LEXER_ERROR_MISSING_SEMI_COLON)
		}
	}
}
func LexSemiColon(lexer *Lexer) LexFn {
	lexer.Pos += len(lexertoken.SEMI_COLON)
	lexer.Emit(lexertoken.TOKEN_SEMI_COLON)
	return LexBegin
}
func LexRareEOF(lexer *Lexer) LexFn {
	lexer.Emit(lexertoken.TOKEN_EOF)
	return LexBegin
}
