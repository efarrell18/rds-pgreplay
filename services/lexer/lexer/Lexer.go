package lexer

import (
	"fmt"
	"github.com/securityscorecard/rds-pgreplay/services/lexer/lexertoken"
	"unicode"
	"unicode/utf8"
)

type Lexer struct {
	Name   string
	Input  string
	Tokens chan lexertoken.Token
	State  LexFn
	Start  int
	Pos    int
	Width  int
	i      int
}

/*
Puts a token onto the token channel. The value of this token is
read from the input based on the current lexer position.
*/
func (this *Lexer) Emit(tokenType lexertoken.TokenType) {
	this.Tokens <- lexertoken.Token{Type: tokenType, Value: this.Input[this.Start:this.Pos]}
	this.Start = this.Pos
}

/*
Increment the position
*/
func (this *Lexer) Inc() {
	this.Pos++
	if this.Pos >= utf8.RuneCountInString(this.Input) {
		this.Emit(lexertoken.TOKEN_EOF)
	}
}

/*
Reads the next rune (character) from the input stream
and advances the lexer position.
*/
func (this *Lexer) Next() rune {
	if this.Pos >= utf8.RuneCountInString(this.Input) {
		this.Width = 0
		return lexertoken.EOF
	}

	result, width := utf8.DecodeRuneInString(this.Input[this.Pos:])

	this.Width = width
	this.Pos += this.Width
	return result
}

/*
Return the next token from the channel
*/
func (this *Lexer) NextToken() lexertoken.Token {
	for {
		select {
		case token := <-this.Tokens:
			return token
		default:
			this.State = this.State(this)
		}
	}

	//panic("Lexer.NextToken reached an invalid state!!")
}

/*
Return a slice of the input from the current lexer position
to the end of the input string.
*/
func (this *Lexer) InputToEnd() string {
	return this.Input[this.Pos:]
}

/*
Skips whitespace until we get something meaningful.
*/
func (this *Lexer) SkipWhitespace() {
	for {
		ch := this.Next()

		if !unicode.IsSpace(ch) {
			this.Dec()
			break
		}

		if ch == lexertoken.EOF {
			this.Emit(lexertoken.TOKEN_EOF)
			break
		}
	}
}

/*
Decrement the position
*/
func (this *Lexer) Dec() {
	this.Pos--
}

/*
Returns a token with error information.
*/
func (this *Lexer) Errorf(format string, args ...interface{}) LexFn {
	this.Tokens <- lexertoken.Token{
		Type:  lexertoken.TOKEN_ERROR,
		Value: fmt.Sprintf(format, args...),
	}

	return nil
}
func (this *Lexer) IsEOF() bool {
	return this.Pos >= len(this.Input)
}
