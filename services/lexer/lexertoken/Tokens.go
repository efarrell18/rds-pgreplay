package lexertoken

const EOF rune = 0

const COLON = ":"
const AT = "@"
const LEFT_BRACKET = "["
const RIGHT_BRACKET = "]"
const SEMI_COLON = ";"
const STATEMENT_COLON = "statement:"
