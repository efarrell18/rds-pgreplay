package errors

const (
	LEXER_ERROR_MISSING_COLON_ONE     string = "The log is missing a colon after timestamp"
	LEXER_ERROR_MISSING_COLON_TWO     string = "The log is missing a colon after ip"
	LEXER_ERROR_MISSING_AT_SIGN       string = "The log is missing at sign after username"
	LEXER_ERROR_MISSING_COLON_THREE   string = "The log is missing at sign after database name"
	LEXER_ERROR_MISSING_LEFT_BRACKET  string = "The log is missing left bracket"
	LEXER_ERROR_MISSING_RIGHT_BRACKET string = "The log is missing RIGHT bracket"
	LEXER_ERROR_MISSING_COLON_FOUR    string = "The log is missing colon after right bracket"
	LEXER_ERROR_MISSING_COLON_FIVE    string = "The log is missing colon after log"
	LEXER_ERROR_MISSING_COLON_SIX     string = "The log is missing colon after statment word"
	LEXER_ERROR_MISSING_SEMI_COLON    string = "The log is missing colon after statment word"
	LEXER_ERROR_REGEXP_ERROR          string = "There was an error with REGEXP"
)
