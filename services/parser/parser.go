package parser

import (
	"github.com/securityscorecard/rds-pgreplay/models/logfile"
	"github.com/securityscorecard/rds-pgreplay/services/lexer"
	"github.com/securityscorecard/rds-pgreplay/services/lexer/lexertoken"
	"strings"
)

func isEOF(token lexertoken.Token) bool {
	return token.Type == lexertoken.TOKEN_EOF
}

func Parse(fileName, input string) logfile.LogFile {
	output := logfile.LogFile{
		FileName: fileName,
		Lines:    make([]logfile.LogLine, 0),
	}

	var token lexertoken.Token
	var tokenValue string

	line := logfile.LogLine{}

	l := lexer.BeginLexing(fileName, input)

	for {
		token = l.NextToken()

		if token.Type != lexertoken.TOKEN_SQL_STATEMENT {
			tokenValue = strings.TrimSpace(token.Value)
		} else {
			tokenValue = token.Value
		}
		if isEOF(token) {
			output.Lines = append(output.Lines, line)
			break
		}

		switch token.Type {
		case lexertoken.TOKEN_TIMESTAMP:
			if line.Statement != "" {
				output.Lines = append(output.Lines, line)
			}
			line = logfile.LogLine{}
			line.Timestamp = tokenValue
		case lexertoken.TOKEN_REMOTE_HOST:
			line.IP = tokenValue
		case lexertoken.TOKEN_USERNAME:
			line.Username = tokenValue
		case lexertoken.TOKEN_DATABASE:
			line.Database = tokenValue
		case lexertoken.TOKEN_PROCESS_ID:
			line.ProcessID = tokenValue
		case lexertoken.TOKEN_LOG_TYPE:
			line.LogType = tokenValue
		case lexertoken.TOKEN_PRE_STATEMENT:
			line.PreStatement = tokenValue
		case lexertoken.TOKEN_SQL_STATEMENT:
			line.Statement = tokenValue
		}
	}
	return output
}
