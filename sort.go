package main

import (
	"github.com/securityscorecard/rds-pgreplay/models/logfile"
)

type ByLineNumber []logfile.LogFile

func (a ByLineNumber) Len() int           { return len(a) }
func (a ByLineNumber) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByLineNumber) Less(i, j int) bool { return a[i].LineNumber < a[j].LineNumber }
