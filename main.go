package main

// Converts %t:%r:%u@%d:[%p]:    to %m|%u|%d|%c|

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/securityscorecard/rds-pgreplay/models/logfile"
	"github.com/securityscorecard/rds-pgreplay/services/parser"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
	"sync"
	"time"
)

type RawLine struct {
	String     string
	LineNumber int
}
type PID map[string]time.Time

var pids PID

const MILLISECOND_POSTGRESQL = "2006-01-02 15:04:05.000 UTC"
const POSTGRESQL_DATETIME = "2006-01-02 15:04:05 UTC"

func main() {
	files, err := ioutil.ReadDir("./")
	if err != nil {
		log.Fatal(err)
	}
	for _, f := range files {
		if strings.HasPrefix(f.Name(), "postgresql.log") && !strings.HasSuffix(f.Name(), "_converted") {
			log.Println(f.Name())
			convert(f.Name())
		}

	}
	return
}
func convert(filename string) {

	inFile, err := os.Open(filename)
	if err != nil {
		log.Println(err)
		return
	}
	defer inFile.Close()
	scanner := bufio.NewScanner(inFile)
	scanner.Split(bufio.ScanLines)
	i := 0
	rawLines := make([]RawLine, 0)
	var buffer bytes.Buffer
	for scanner.Scan() {
		if strings.TrimSpace(scanner.Text()) == "" {
			// This makes sure that blank lines don't get added.
			// Basically it needs to already have something in buffer.
			if buffer.String() != "" {
				rawLines = append(rawLines, RawLine{buffer.String(), i})
				buffer.Reset()
				i++
			}
		} else {
			buffer.WriteString(scanner.Text() + "\n")
		}
	}
	if strings.TrimSpace(buffer.String()) != "" {
		rawLines = append(rawLines, RawLine{buffer.String(), i})
		buffer.Reset()
	}

	log.Println("Starting lexer and parser for the", filename, "...")

	logFiles := concurrentConverter(filename, rawLines)
	//logFiles := singleConverter(filename, rawLines)

	// Sort
	sort.Sort(ByLineNumber(logFiles))

	writeFilename := fmt.Sprintf("%s_converted", filename)
	file, err := os.Create(writeFilename)
	if err != nil {
		log.Println(err)
		return
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	// Here rewrite Logs
	// Converts %t:%r:%u@%d:[%p]:    to %m|%u|%d|%c|
	i2 := 0
	pids = make(map[string]time.Time)
	for _, line := range logFiles {
		log.Println(line.LineNumber)
		for _, l := range line.Lines {
			t, err := time.Parse(POSTGRESQL_DATETIME, l.Timestamp)
			if err != nil {
				log.Println(err)
				continue
			}
			t = t.Add(time.Millisecond * time.Duration(i2))
			timeString := t.Format(MILLISECOND_POSTGRESQL)
			fmt.Fprint(w, timeString)
			if _, ok := pids[l.ProcessID]; ok {
				t = pids[l.ProcessID]
			} else {
				pids[l.ProcessID] = t
			}
			fmt.Fprint(w, "|")
			fmt.Fprint(w, l.Username)
			fmt.Fprint(w, "|")
			fmt.Fprint(w, l.Database)
			fmt.Fprint(w, "|")
			// SELECT to_hex(trunc(EXTRACT(EPOCH FROM backend_start))::integer) || '.' ||
			//to_hex(pid)
			//FROM pg_stat_activity;
			fmt.Fprint(w, fmt.Sprintf("%s.%s", fmt.Sprintf("%x", int(t.Unix())), fmt.Sprintf("%s", l.ProcessID)))
			fmt.Fprint(w, "|")
			fmt.Fprint(w, l.LogType)
			fmt.Fprint(w, ":  ")
			fmt.Fprint(w, l.PreStatement)
			fmt.Fprint(w, ":") // After pre-statment, pgreplay expects 1 space
			fmt.Fprintln(w, l.Statement)
			i2++
		}
		if i2 >= 999 {
			i2 = 0
		}
	}
	w.Flush()
}
func concurrentConverter(filename string, rawLines []RawLine) []logfile.LogFile {
	// concurrency
	LogFileChan := make(chan logfile.LogFile)
	var wg sync.WaitGroup
	wg.Add(len(rawLines)) //2)//len(rawLines))
	logFiles := make([]logfile.LogFile, 0)
	for _, rawLine := range rawLines {
		go func(rawLine RawLine) {
			defer wg.Done()
			parsedLogFile := parser.Parse(filename, rawLine.String)
			parsedLogFile.LineNumber = rawLine.LineNumber
			LogFileChan <- parsedLogFile
		}(rawLine)
	}
	for _, _ = range rawLines {
		logFile := <-LogFileChan
		log.Println(logFile.LineNumber)
		logFiles = append(logFiles, logFile)
	}
	wg.Wait()
	return logFiles
}
func singleConverter(filename string, rawLines []RawLine) []logfile.LogFile {
	// concurrency
	logFiles := make([]logfile.LogFile, 0)
	for _, rawLine := range rawLines {
		parsedLogFile := parser.Parse(filename, rawLine.String)
		parsedLogFile.LineNumber = rawLine.LineNumber
		logFiles = append(logFiles, parsedLogFile)
	}
	return logFiles
}
